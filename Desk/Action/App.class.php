<?php	 
class App extends Action{
	public function __construct() {
//		 $struct=require(EXTEND . 'Struct.php');
//		 print_r($struct);
//		 $this->db->refreshStruct($struct);
		$auth=_instance('Action/Auth');
		 
	}		
	//得到所有应用管理
	public function sapp(){
		$sApp= $this->db()->fetchAll("select * from app",'appid');
		 echo json_encode($sApp);
	}

	public function app_show(){
		$sql=" select * from app order by appid desc";		
		$list=$this->db()->fetchAll($sql);
		$this->assign(array("list"=>$list));
		$this->show('app');	
	}
	public function app_modify(){
		$appid = ($_POST["appid"])?$_POST["appid"]:$_GET["appid"];
		if(empty($_POST)){
			$sql   = "select * from app  where appid = '$appid';";		
			$one   = $this->db()->fetchOne($sql);
			$this->assign(array("one"=>$one));
			$this->show('app_modify');			
		}else{
			$sql   = "update app set 
					  name='$_POST[name]',
					  icon='$_POST[icon]',
					  url='$_POST[url]',
					  width='$_POST[width]',
					  height='$_POST[height]',
					  remark='$_POST[remark]'
					where appid = '$appid';";
					
			$one   = $this->db()->query($sql);
			$this->location("",'App/app_show',1);					
		}
	}

	public function app_install(){
		if(empty($_SESSION["deskos"]["username"])){
			echo "0";
		}else{
			$appid 	 =$this->_REQUEST("appid");
			$deskarr =explode(",",$_SESSION["deskos"]["desk1"]);
			$desknum =$_SESSION["deskos"]['desk1'];
			if(!in_array($appid,$deskarr)){
				$desknum=$_SESSION["deskos"]['desk1'].",".$appid;
			}
			$sql 	= "update manager set desk1='$desknum' where username='".$_SESSION["deskos"]["username"]."'";
			if($this->db()->query($sql)){
				$_SESSION["deskos"]['desk1']=$desknum;	
			}	
			echo "1";	
		}
	}
 	/**
    * 获得所有的查询数据，并且通选择框形式返回
    * @access function
    * @param string $inputname  选择框名字
    * @param int $id  默认选择中
    * @return string 选择框
    */
	public function app_checked($inputname,$id=null){
		$id_arr = (!is_array($id))?explode(",",$id):0;
		$sql	=" select * from app order by appid desc";		
		$list	=$this->db()->fetchAll($sql);//查询结果为二维数组，需foreach循环
		$string = "";
		if(is_array($list)){
			foreach($list as $key=>$row){
				$string .="<li style='list-style:none;width:150px;float:left;'><input type='checkbox' name='".$inputname."[]' value='".$row["appid"]."' " ;
				if(in_array($row["appid"],$id_arr)) $string .=" checked";
				$string .="> ".$row["name"];
				$string .= "</li>";
			}
		}
		return $string;
	}
	/*********************************************************************
	 * 根据传入的ID值查询出相对的名称
	 * ex:$id = 1,3,5, 
	*/
	
	public function app_get_name($id){
		$sql  ="select name from app where appid in ($id)";	
		$list =$this->db()->fetchAll($sql);//查询结果为二维数组，需foreach循环
		$str  ="";
		if(is_array($list)){
			foreach($list as $row){
				$str .= "|-".$row["name"]."&nbsp;";
			}
		}
		return $str;
	}
	
}//

?>
