<?php include(VIEW."header_art.php"); ?>
<body>
<div class="widget-box">
  <div class="widget-title">
    <ul class="nav nav-tabs">
      <li class=active><a data-toggle="tab" href="#tab<?php echo $key;?>"><?php echo $one["name"];?></a></li>
    </ul>
  </div>
  <form action="<?=ACT?>/Network/network_modify_save" method="post" name="myform" class="form-horizontal" />
  <div class="widget-content tab-content">
    <div id="tab" class="tab-pane active">
      <div class="widget-content nopadding">
        <div class="control-group">
          <label class="control-label">标识：</label>
          <div class="controls">
            <input type="text" name="name" id="name" value="<?php echo $one["name"]?>"/>
            <input type="hidden" name="ifname" id="ifname" value="<?php echo $one["ifname"]?>"/>
            <span class="help-block">接口物理名为：<?php echo $one["ifname"]?></span> </div>
			</div>
        </div>
        <div class="control-group">
          <label class="control-label">类型：</label>
          <div class="controls">
            <select id="type" name="type" onChange="setChange(this.value)">
            	<option value="DHCP" <?php if($one["type"]=="DHCP") echo "selected"?> >DHCP</option>
                <option value="PPPOE" <?php if($one["type"]=="PPPOE") echo "selected"?>>PPPOE</option>
                <option value="STATIC" <?php if($one["type"]=="STATIC") echo "selected"?>>STATIC</option>
            </select>
			</div>
        </div>
        <div class="control-group">
          <label class="control-label">MAC 地址：</label>
          <div class="controls">
            <input type="text" name="mac" id="mac" value="<?php echo $one["mac"]?>"/>
			</div>
        </div>
        <div class="tab_static">
        
            <div class="control-group">
              <label class="control-label">IP 地址：</label>
              <div class="controls">
                <input type="text" name="ipaddr" id="ipaddr" value="<?php echo $one["ipaddr"]?>"/>
                </div>
            </div>
            <div class="control-group">
              <label class="control-label">掩码地址：</label>
              <div class="controls">
                <input type="text" name="subnet" id="subnet" value="<?php echo $one["subnet"]?>"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">网关地址：</label>
              <div class="controls">
                 <input type="text" name="gateway" id="gateway"  value="<?php echo $one["gateway"]?>"/>
              </div>
            </div>
        </div>
        <div class="tab_pppoe">
            <div class="control-group">
              <label class="control-label">用户名：</label>
              <div class="controls">
                 <input type="text" name="username" id="username"  value="<?php echo $one["username"]?>"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">密 码：</label>
              <div class="controls">
                 <input type="text" name="password" id="password"  value="<?php echo $one["password"]?>"/>
              </div>
            </div>
        </div>

        <div class="form-actions">
          <button type="button" id="network_btn" class="btn btn-primary">保存</button>
          <button type="button" class="btn btn-primary" onClick="javascript:window.history.go(-1);">返回</button>
        </div>
      </div>
    </div>
  </div>
  </form>
</div>
</body>
</html>
<script  type="text/javascript">
type	=$("#type").val();
window.onload=setChange(type);
function  setChange(value) {   
	if(value=="DHCP"){
		$(".tab_static").css('display','none');
		$(".tab_pppoe").css('display','none');
	}else if(value=="PPPOE"){
		$(".tab_static").css('display','none');
		$(".tab_pppoe").css('display','block');		  
	}else if(value=="STATIC"){
		$(".tab_static").css('display','block');
		$(".tab_pppoe").css('display','none');	  
	}
	 
}   
$(document).ready(function(){
  $("#network_btn").click(function(){
	ifname	=$("#ifname").val();
	name	=$("#name").val();
	mac		=$("#mac").val();
	type	=$("#type").val();
	ipaddr 	=$("#ipaddr").val();
	subnet	=$("#subnet").val();
	gateway	=$("#gateway").val();
	username=$("#username").val();
	password=$("#password").val();
	$.ajax({
		type:'post',//可选
		url:'<?php echo ACT ?>/Network/network_modify_save/',//这里是接收数据的PHP程序
		data:{
			ifname:ifname,	
			name:name,	
			mac:mac,
			type:type,
			ipaddr:ipaddr,
			subnet:subnet,
			gateway:gateway,
			username:username,
			password:password
		},//传给PHP的数据，多个参数用&连接
		dataType:'text',
		cache: false,        
		async: false,		
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
				art.dialog({
					content: '保存成功',
					icon: 'succeed',
					fixed: false,
					lock: true,
					max:false,
					min:false,
					time: 1.5
				});
			}
		});	
  });
  $("#dns_btn").click(function(){
	dns1	=$("#dns1").val();
	dns2 	=$("#dns2").val();
	dns3	=$("#dns3").val();
	$.ajax({
		type:'post',//可选
		url:'<?php echo ACT ?>/Config/set_dns/',//这里是接收数据的PHP程序
		data:{
			dns1:dns1,	
			dns2:dns2,
			dns3:dns3,
		},//传给PHP的数据，多个参数用&连接
		dataType:'text',
		cache: false,        
		async: false,		
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
				art.dialog({
					content: '保存成功',
					icon: 'succeed',
					fixed: false,
					lock: true,
					max:false,
					min:false,
					time: 1.5
				});
			}
		});	
  }); 
  
});
</script>