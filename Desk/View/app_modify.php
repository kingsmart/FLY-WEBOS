<?php include("header.php"); ?>
<body>
	<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>应用管理-修改</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="<?php echo ACT?>/App/app_modify/appid/<?php echo $one["appid"]?>" method="post" class="form-horizontal" />
									<div class="control-group">
										<label class="control-label">名称</label>
										<div class="controls">
											<input type="text" name="name" value="<?php echo $one["name"]?>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">图标</label>
										<div class="controls">
											<input type="text" name="icon" value="<?php echo $one["icon"]?>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">地址</label>
										<div class="controls">
											<input type="text" name="url" value="<?php echo $one["url"]?>"/>
											<span class="help-block">这是应用窗口打开链接地址</span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">窗口</label>
										<div class="controls">
											宽：<input type="text" size="15" name="width"  style="width:30px;"  value="<?php echo $one["width"]?>"/>px
											&nbsp;
											
											高：<input type="text" size="15" name="height"  style="width:30px;" value="<?php echo $one["height"]?>"/>px
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">描述</label>
										<div class="controls">
											<textarea name="remark"><?php echo $one["remark"]?></textarea>
										</div>
									</div>
									<div class="form-actions">
										<button type="submit" class="btn btn-primary">保存</button>
										<button type="button" class="btn btn-primary" onClick="javascript:window.history.go(-1);">返回</button>
									</div>
								</form>
							</div>
						</div>
</body>
</html>
