<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Free HTML5 Bootstrap Admin Template</title>

	<!-- The styles -->
	<link id="bs-css" href="../../ui/html5/css/bootstrap-cerulean.css" rel="stylesheet">
	<link href="../../ui/html5/css/bootstrap-responsive.css" rel="stylesheet">
	<link href="../../ui/html5/css/charisma-app.css" rel="stylesheet">
	<link href="../../ui/html5/css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href='../../ui/html5/css/fullcalendar.css' rel='stylesheet'>
	<link href='../../ui/html5/css/fullcalendar.print.css' rel='stylesheet'  media='print'>
	<link href='../../ui/html5/css/chosen.css' rel='stylesheet'>
	<link href='../../ui/html5/css/uniform.default.css' rel='stylesheet'>
	<link href='../../ui/html5/css/colorbox.css' rel='stylesheet'>
	<link href='../../ui/html5/css/jquery.cleditor.css' rel='stylesheet'>
	<link href='../../ui/html5/css/jquery.noty.css' rel='stylesheet'>
	<link href='../../ui/html5/css/noty_theme_default.css' rel='stylesheet'>
	<link href='../../ui/html5/css/elfinder.min.css' rel='stylesheet'>
	<link href='../../ui/html5/css/elfinder.theme.css' rel='stylesheet'>
	<link href='../../ui/html5/css/jquery.iphone.toggle.css' rel='stylesheet'>
	<link href='../../ui/html5/css/opa-icons.css' rel='stylesheet'>
	<link href='../../ui/html5/css/uploadify.css' rel='stylesheet'>
</head>

<body>
<?php 
/**
 * 提示函数.并且跳转到指定的地址
 * 
 * @param $msg,$usr ，$usrl
 * retrun IP 
 *
 */

function msg_print($msg ='',$url='') {//提示输出函数
	if(is_array($msg)){
		foreach($msg as $va_msg){
			$tmp_msg .="<li>".$va_msg."</li>";
		}
	}else{
			$tmp_msg = $msg;
	}

echo <<<EOT
<table border="0" width="50%" height="300" align="center" valign="middle">
  <tr>
    <td>

		<div class="row-fluid sortable">
		<div class="box span3">
							<div class="box-header well" data-original-title>
								<h2><i class="icon-th"></i> {$msg}</h2>
							</div>
							<div class="box-content">
							<div class="row-fluid">
									系统将在<span id="totalSecond">3</span>秒钟后自动跳转，如果页面没有跳转请点击<a href='$url' style='color:#ff0000'>【这里】</a>
							</div>                   
						  </div>
						</div>
		</div>
</td>
  </tr>
</table>
EOT;


echo <<<EOT
<script language="javascript" type="text/javascript">  
<!--   
    var second = document.getElementById('totalSecond').textContent;   
    if (navigator.appName.indexOf("Explorer") > -1)  //判断是IE浏览器还是Firefox浏览器，采用相应措施取得秒数   
    {   
        second = document.getElementById('totalSecond').innerText;   
    } else {   
        second = document.getElementById('totalSecond').textContent;   
    }   
    setInterval("redirect()", 1000);  //每1秒钟调用redirect()方法一次   
    function redirect()   {   
        if (second < 0){   
            location.href = '$url';   
        }else{   
            if (navigator.appName.indexOf("Explorer") > -1){   
                document.getElementById('totalSecond').innerText = second--;   
            }else{   
                document.getElementById('totalSecond').textContent = second--;   
            }   
        }   
    }   
-->  
</script> 
EOT;
    exit; 
}
?>