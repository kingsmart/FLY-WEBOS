<?php include("header.php"); ?>
<body>
<div class="widget-box">
  <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
    <h5>用户列表</h5>
  </div>
  <div class="widget-content nopadding">
    <table width="100%" class="table table-bordered table-striped">
      <tbody>
        <tr>
          <td align="center" valign="middle">帐号</td>
          <td>密码</td>
          <td>所属权限</td>
          <td>注册时间</td>
          <td>操作</td>
        </tr>
        <?php foreach ($list as $key=>$v) { ?>
        <tr>
          <td align="center" valign="middle"><?php echo $v["username"]?></td>
          <td><?php echo $v["password"]?></td>
          <td><?php echo $role[$v["id"]]?></td>
          <td><?php echo $v["regdt"]?></td>
          <td><a href="<?php echo ACT?>/Manager/manager_modify/id/<?php echo $v["id"]?>">
            <button class="btn btn-primary"><i class="icon-pencil icon-white"></i> Edit</button>
            </a> <a href="<?php echo ACT?>/Manager/manager_del/id/<?php echo $v["id"]?>">
            <button class="btn btn-danger"><i class="icon-remove icon-white"></i> Delete</button>
            </a> </td>
        </tr>
        <?php }?>
        <tr>
          <td colspan="5" align="center" valign="middle"><a href="<?php echo ACT?>/Manager/manager_add">
            <button type="button" class="btn btn-primary">添加</button>
            </a>
            <?php echo $page?></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
</body>
<script>
 function trim(str){ //删除左右两端的空格
     return str.replace(/(^\s*)|(\s*$)/g, "");
 }
 function ltrim(str){ //删除左边的空格
     return str.replace(/(^\s*)/g,"");
 }
 function rtrim(str){ //删除右边的空格
     return str.replace(/(\s*$)/g,"");
 }
function app_install(appid){
	$.ajax({
		type:'post',//可选
		url:'<?php echo ACT ?>/App/app_install/appid/'+appid,//这里是接收数据的PHP程序
		data:'',//传给PHP的数据，多个参数用&连接
		dataType:'text',
        cache: false,        
		async: false,		
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
				n=trim(msg);
				if(n=0){
					  // var iframe = this.iframe.contentWindow;
          			   //iframe.document.getElementById("btnsave").click();
					  
				}
			},
		});	
		
		
}
</script>
</html>